import 'package:flutter/material.dart';

enum APP_THEME { LIGHT, DARK }

void main() {
  runApp(ContactProfilePage());
}

class MyAppTheme {
  static ThemeData appThemeLight() {
    return ThemeData(
        brightness: Brightness.light,
        appBarTheme: AppBarTheme(
            color: Colors.cyan, iconTheme: IconThemeData(color: Colors.white)),
        listTileTheme: ListTileThemeData(iconColor: Colors.blueAccent));
  }

  static ThemeData appThemeDark() {
    return ThemeData(
        brightness: Brightness.dark,
        appBarTheme: AppBarTheme(
            color: Colors.cyan, iconTheme: IconThemeData(color: Colors.white)),
        listTileTheme: ListTileThemeData(iconColor: Colors.blueAccent));
  }
}

class ContactProfilePage extends StatefulWidget {
  @override
  State<ContactProfilePage> createState() => _ContactProfilePageState();
}

class _ContactProfilePageState extends State<ContactProfilePage> {
  var currentTheme = APP_THEME.LIGHT;
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: currentTheme == APP_THEME.DARK
          ? MyAppTheme.appThemeLight()
          : MyAppTheme.appThemeDark(),
      home: Scaffold(
        appBar: buildAppBarWidget(),
        body: buildBodyWidget(),
        floatingActionButton: FloatingActionButton.small(
          onPressed: () {
            setState(() {
              currentTheme == APP_THEME.DARK
                  ? currentTheme = APP_THEME.LIGHT
                  : currentTheme = APP_THEME.DARK;
            });
          },
          child: currentTheme == APP_THEME.DARK
              ? Icon(Icons.sunny)
              : Icon(Icons.nightlight),
          backgroundColor: Colors.blue,
        ),
      ),
    );
  }
}

Widget buildCallButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.call,
        ),
        onPressed: () {},
      ),
      Text("Call"),
    ],
  );
}

Widget buildTextButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.textsms,
        ),
        onPressed: () {},
      ),
      Text("Text"),
    ],
  );
}

Widget buildVideoButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.video_call_rounded,
        ),
        onPressed: () {},
      ),
      Text("Video"),
    ],
  );
}

Widget buildEmailButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.email,
        ),
        onPressed: () {},
      ),
      Text("Email"),
    ],
  );
}

Widget buildDirectionsButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.directions,
        ),
        onPressed: () {},
      ),
      Text("Directions"),
    ],
  );
}

Widget buildPayButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.attach_money,
        ),
        onPressed: () {},
      ),
      Text("Pay"),
    ],
  );
}

Widget mobilePhoneListTile() {
  return ListTile(
    leading: Icon(
      Icons.call,
      color: Colors.grey,
    ),
    title: Text("+66-63-448-8262"),
    subtitle: Text("mobile"),
    trailing: IconButton(
      onPressed: () {},
      icon: Icon(
        Icons.textsms,
      ),
    ),
  );
}

Widget otherPhoneListTile() {
  return ListTile(
    leading: Text(""),
    title: Text("+66-86-3544-950"),
    subtitle: Text("other"),
    trailing: IconButton(
      onPressed: () {},
      icon: Icon(
        Icons.textsms,
      ),
    ),
  );
}

Widget emailListTile() {
  return ListTile(
    leading: Icon(
      Icons.email,
      color: Colors.grey,
    ),
    title: Text("thana.don@gmail.com"),
    subtitle: Text("work"),
  );
}

Widget addressListTile() {
  return ListTile(
    leading: Icon(
      Icons.location_on,
      color: Colors.grey,
    ),
    title: Text("75/7 Bangsaen Meung Chonburi"),
    subtitle: Text("home"),
    trailing: IconButton(
      onPressed: () {},
      icon: Icon(
        Icons.directions,
      ),
    ),
  );
}

PreferredSizeWidget buildAppBarWidget() {
  return AppBar(
    leading: Icon(
      Icons.arrow_back,
    ),
    actions: <Widget>[
      IconButton(onPressed: () {}, icon: Icon(Icons.star_border))
    ],
  );
}

Widget buildBodyWidget() {
  return ListView(
    children: <Widget>[
      Column(
        children: <Widget>[
          Container(
            width: double.infinity,

            //Height constraint at Container widget level
            height: 250,

            child: Image.network(
              "https://i.pinimg.com/originals/28/db/2b/28db2b74bd15a85c1f72d9bca4981d6a.jpg",
              fit: BoxFit.cover,
            ),
          ),
          Container(
            height: 60,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Text("Thanadon Boontheung",
                      style: TextStyle(fontSize: 30)),
                ),
              ],
            ),
          ),
          Divider(
            color: Colors.grey,
          ),
          Container(
            margin: EdgeInsets.only(top: 8, bottom: 8),
            child: Theme(
              data: ThemeData(
                iconTheme: IconThemeData(
                  color: Colors.lightBlueAccent,
                ),
              ),
              child: profileActionItems(),
            ),
          ),
          Divider(
            color: Colors.grey,
          ),
          Container(
            margin: EdgeInsets.all(10.0),
            child: mobilePhoneListTile(),
          ),
          Container(
            height: 60,
            margin: EdgeInsets.all(10.0),
            child: otherPhoneListTile(),
          ),
          Divider(
            color: Colors.grey,
          ),
          Container(
            margin: EdgeInsets.all(10.0),
            child: emailListTile(),
          ),
          Divider(
            color: Colors.grey,
          ),
          Container(
            margin: EdgeInsets.all(10.0),
            child: addressListTile(),
          )
        ],
      )
    ],
  );
}

Widget profileActionItems() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: <Widget>[
      buildCallButton(),
      buildTextButton(),
      buildVideoButton(),
      buildEmailButton(),
      buildDirectionsButton(),
      buildPayButton()
    ],
  );
}
